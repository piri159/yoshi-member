const express = require("express");
const app = express();
const axios = require("axios");
const cheerio = require("cheerio");

const port = 8080;

app.get("/", (req, res) => {
  const getHtml = async (a) => {
  try {
    return await axios.get("https://maplestory.nexon.com/Common/Guild?gid=285951&wid=1&orderby=0&page="+a);
  } catch (error) {
    console.error(error);
  }
};

let arrName = [];

var testFunction = async () => {
  for(var a = 1; a < 10; a++){
    getHtml(a).then(html => {
      const $ = cheerio.load(html.data);
      for(var b = 0; b<$(".rank_table tr").length-1; b++){
        arrName.push($(".rank_table .left a").eq(b).text());
      }
    })
  }
}

testFunction()
})


app.listen(port, ()=>{
  console.log(`Yoshi Member App listening on port ${port}`);
})
